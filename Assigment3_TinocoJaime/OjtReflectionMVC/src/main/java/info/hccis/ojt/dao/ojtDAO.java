/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.dao;

import info.hccis.ojt.entity.OjtReflection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author jtinocotejeida
 */
public class ojtDAO {
    private final static Logger LOGGER = Logger.getLogger(ojtDAO.class.getName());
    
    /*
      Default constructor will setup the connection and statement objects to be
        used by this OjtDAO instance
    
    */
    
    
     public static ArrayList<OjtReflection> selectAll() {

        ArrayList<OjtReflection> students = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getDBConnection();

            sql = "SELECT * FROM ojtreflection order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                int studentId = rs.getInt("studentId");
                String name = rs.getString("studentName");
                String reflection = rs.getString("reflection");
                OjtReflection student = new OjtReflection(id, studentId, name,reflection);
                
                students.add(student);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return students;
    }
    /*
     this method show particular student based on registration id
    */
    
   
     public static OjtReflection select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        OjtReflection student = null;
        try {
            conn = ConnectionUtils.getConnection();
            System.out.println("Loading for: " + idIn);
            sql = "SELECT * FROM ojtreflection where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt(1);
                int studentId = rs.getInt("studentId");
                String name = rs.getString("studentName");
                String reflection = rs.getString("reflection");
                student = new OjtReflection(id, studentId, name,reflection);
                
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return student;
    }
     /**
     * This method will delete the student associated with the student id.
     *
     * @param studentId Identifies the student to be deleted.
     * 
     * @author Divya S
     */
    public static synchronized void delete(int idnum) throws Exception  {
         PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM `ojtreflection` WHERE id=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idnum);

            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }

    }

    
    
    
    public static synchronized OjtReflection update(OjtReflection student) throws Exception {
//        System.out.println("inserting camper");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if camper exists.
            //Note that the default for an Integer is null not 0  !!!
            //When I try to compare the getId() to null it throws an exception.
            
            int studentIdInt;
            try {
                studentIdInt = student.getId();
            } catch (Exception e) {
                studentIdInt = 0;

            }

            if (studentIdInt == 0) {

                sql = "SELECT max(id) from ojtreflection";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                student.setId(max);

                sql = "INSERT INTO `ojtreflection`(`id`, `studentId`, `studentName`, `reflection`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, student.getId());
                ps.setInt(2, student.getStudentId());
                ps.setString(3, student.getStudentName());
                ps.setString(4, student.getReflection());

            } else {

                 sql = "UPDATE `ojtreflection` SET `studentId`=?,`studentName`=?,`reflection`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, student.getStudentId());
                ps.setString(2, student.getStudentName());
                ps.setString(3, student.getReflection());
                ps.setInt(4, student.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return student;

    }
}
