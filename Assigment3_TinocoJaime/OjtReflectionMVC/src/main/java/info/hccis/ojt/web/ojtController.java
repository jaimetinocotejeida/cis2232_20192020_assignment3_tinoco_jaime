/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.web;

import info.hccis.ojt.dao.ojtDAO;
import info.hccis.ojt.entity.OjtReflection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jtinocotejeida
 */
public class ojtController {
    
    
    @RequestMapping("/ojt/add")
    public String studentAdd(Model model) {

        //put a Ojt reflection object in the model to be used to associate with the input tags of 
        //the form.
        OjtReflection student = new OjtReflection();
        model.addAttribute("studentadd", student);

        //This will send the user to the welcome.html page.
        return "ojt/add";
    }
    
     @RequestMapping("/ojt/update")
    public String studentUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println(" id passed=" + idToFind);
        //- It will go to the database and load the camper details into a camper
        //  object and put that object in the model.  
        OjtReflection modifyStudent = ojtDAO.select(Integer.parseInt(idToFind));

        model.addAttribute("student", modifyStudent);
        return "/ojt/add";

    }
     @RequestMapping("/ojt/delete")
    public String studentDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println(" id passed=" + idToFind);
        try {
            //- It will go to the database and load the student details into a OJTreflection
            //  object and put that object in the model.
            ojtDAO.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }

        //Reload the students list so it can be shown on the next view.
        model.addAttribute("students", ojtDAO.selectAll());
        return "/ojt/list";

    }

   @RequestMapping("/ojt/addSubmit")
    public String studentAddSubmit(Model model, @ModelAttribute("student") OjtReflection theStudentFromTheForm) {

     
        
//        if (result.hasErrors()) {
//            System.out.println("Error in validation.");
//            String error = "Validation Error";
//            model.addAttribute("message", error);
//            return "ojt/add";
//        }
        //Call the dao method to put this guy in the database.
        
        try {
            ojtDAO.update(theStudentFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("-Did we get here?");
        //Reload the students list so it can be shown on the next view.
        model.addAttribute("students", ojtDAO.selectAll());
           
         
        //This will send the user to the welcome.html page.
        return "ojt/list";
    }
    

}
